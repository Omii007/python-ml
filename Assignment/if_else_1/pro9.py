
''' 9) WAP to check whether the input character is a vowel or consonant.
I/P : a
O/P : vowel
I/P : b
O/P : consonant
'''

char = input("Enter a character: ")

if(char == 'a'):
    print("Vowel")

elif(char == 'e'):
    print("Vowel")

elif(char == 'i'):
    print("Vowel")

elif(char == 'o'):
    print("Vowel")

elif(char == 'u'):
    print("Vowel")

else:
    print("Consonant")
