
''' 3) WAP to find whether the number is even or odd.
I/P : 4
O/P : 4 is an even number.
'''

x = int(input("Enter a number: "))

if(x % 2 == 0):
    
    print("{} is an even number".format(x))

else:
    print("{} is an odd number".format(x))

