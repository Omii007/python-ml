
''' 5) WAP to take an integer ranging from 0 to 6 and print corresponding weekday(week starting from Monday )
I/P : 2
O/P : Wednesday.
'''

day = int(input("Enter a number: "))

if(day == 0):
    print("Monday")

elif(day == 1):
    print("Tuesday")

elif(day == 2):
    print("Wednesday")

elif(day == 3):
    print("Thursday")

elif(day == 4):
    print("Friday")

elif(day == 5):
    print("Saturday")

elif(day == 6):
    print("Sunday")

else:
    print("Plz enter number between 0 to 6")
