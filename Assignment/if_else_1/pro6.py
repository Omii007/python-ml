
''' 6) WAP to check whether the character is Alphabet or not.
I/P : v
O/P : v is an alphabet.
'''

char = int(input("Enter a number: "))

if(char >= 65 and char <= 122):

    print(chr(char),"is an alphabet")

else:
    
    print(chr(char)," is not alphabet")
