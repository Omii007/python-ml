
''' 4) WAP to check whether the number is divisible by 5 or not.
I/P : 55
O/P : 55 is divisible by 5.
'''

x = int(input("Enter a number: "))

if(x % 5 == 0):
    
    print("{} is divisible by 5".format(x))

else:
    
    print("{} is not divisible by 5".format(x))

