
''' 2) WAP to check whether the number is negative, positive or equal to zero.
I/P : -2
O/P : -2 is the negative number. 
'''

x = int(input("Enter a number: "))

if(x > 0):
    print("{} is a positive number".format(x))
elif(x < 0):
    print("{} is a negative number".format(x))
else:
    print("{} is equal to zero".format(x))
