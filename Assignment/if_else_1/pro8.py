
''' 8) WAP to check whether the number is greater than 10 or not
I/P : 12
O/P : yes 12 is greater than 10
I/P : 2
O/P : no 2 is less than 10
'''

num = int(input("Enter a number:"))

if(num > 10):
    print("yes {} is greater than 10".format(num))

else:
    print("no {} is less than 10".format(num))
