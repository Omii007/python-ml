
# Methods of Set :
'''
1) add()
2) copy()
3) difference()
4) difference_update()
5) discard()
6) intersection()
7) intersection_update()
8) isdisjoint()
9) issubset()
10) issuperset()
11) symmetric_difference()
12) symmetric_difference_update()
13) union()
14) update()
15) pop()
16) remove()
17) clear()
'''

setData = {1,2,3,4}
#add()
setData.add(5)
print(setData)

#copy()
setData2 = setData.copy()
print(setData2)

#difference()
setData1 = {1,2,3,4}
setData3 = {3,4,5,6}

setData4 = setData1.difference(setData3)
print(setData4)

#difference_update() return type void mnje None

setData1 = {1,2,3,4}
setData3 = {3,4,5,6}

print(setData1)
print(setData3)

setData1.difference_update(setData3)  # setData1 & setData3 madhil comman data discard krto.
print(setData1)
print(setData3)

# discard()

setData1 = {1,2,3,4}
setData1.discard(3)
print("------")
print(setData1)

# intersection()

setData1 = {1,2,3,4}
setData3 = {3,4,5,6}
setData4 = setData1.intersection(setData3)
print(setData4)

# intersection_update()

setData1 = {1,2,3,4}
setData3 = {3,4,5,6}

setData1.intersection_update(setData3)
print(setData1)

# isdisjoint() common asel tr -> false & difference asel tr -> true

data1 = {1,2,3}
data2 = {4,5,6}
print(data1.isdisjoint(data2))

# issubset() ::all data setData 2 madhe ahe ka check krto return boolean value

setData1 = {1,2,3,4}
setData3 = {3,4,5,6}

print(setData1.issubset(setData3))

# issuperset() :: setData2 cha data setData1 madhe ahe ka check krun boolean return krto

print(setData1.issuperset(setData3))

# symmetric_difference()

setData5 = setData1.symmetric_difference(setData3)
print(setData5)

# symmetric_difference_update()

setData1.symmetric_difference_update(setData3)

print(setData1)
print(setData3)

# union() :: new set madhe thevto.

setData4 = setData1.union(setData3)
print(setData4)

# update()

setData1.update(setData3)
print(setData1)
print(setData3)

#pop() :: parameter dyava lagt nahi
# HashTable chya order nusar 1st data delete krto.

setData1 = {1,2,3,4}
setData1.pop()
print(setData1)

# remove() :: parameter dyava lagto konta data delete krychy to

setData1.remove(4) # data set madhe available nasel tr KeyError deto.
print(setData1)

# clear()

setData1.clear()
print(setData1)
