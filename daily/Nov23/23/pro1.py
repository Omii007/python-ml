
# 19 lecture.
'''
Set : 
1)set madhe duplicate data chalat nahi.
2)set ha class ahe. python madhe sagle class ch ahet.
3) set Internally HashTable cha vapar data thevnyasathi krto. aani HashTable Internally Hash Function cha vapar krto.
3)String Internally Hash Function la call krto.
4)Pratyek veles String la navin value meltat mnun tyancha sequence same nasto.
5)Set ha mutable ahe. Change krta yeto.
6) List aani Dic mutable ahet pn Tuple ha Inmutable ahe(change hota nahi)
7)frozenset() madhe data add krta yet nahi karan tyachya kade add() navachi method nahi.
8) frozenset la data Iterable type cha lagto.
9) HashTable Internally 32 bucket cha use krto.

lis -> []
tuple -> ()
dic -> {key,value}
set -> {10}
'''

setData1 ={}
print(setData1)
print(type(setData1))

setData2 = {10}
print(setData2)
print(type(setData2))

setData3 = {10,20,30,40,50,60,70,80,90,100}
print(setData3);

setData4 = {1,"A",10.5,0,True,False}
print(setData4) # True & False print hota nahi karan True and False internally 1 & 0 ahet. 

setData5 = {"Ashish","Kanha","Badhe","Rahul"}
print(setData5)

setData6 = set("Shashi Sir")
print(setData6)

setData7 = set({20}) # set() yala Iterable type cha data lagto mnje 1 ka peksha jast.to pn list,dic type madhe deu shkto.
print(setData7)
