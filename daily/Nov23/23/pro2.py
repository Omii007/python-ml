

# Access and Add :

# set Index through access krta yet nahi
'''
setData1 = {1,2,3,4,5}
print(setData1[2])  # set object is not subscriptable 

setData1[2] = 50 # set does not support item assignment
'''
setData2 = {1,2,3,4,5}

setData2.add(6)
print(setData2)
