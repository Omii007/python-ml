

def outer(x,y):

    def inner(a,b):

        print("In Inner function")
        return a+b
    print("In outer function")
    print(x+y)
    return inner

retobj = outer(5,8)
ret = retobj(10,15)

print(retobj)
print(ret)
