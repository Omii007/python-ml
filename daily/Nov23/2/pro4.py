

# Function Returning Function:

def outer():

    def inner():
        
        print(id(inner))
        print("In Inner function")
    
    print("In outer function")
    return inner

retobj = outer()

print(id(retobj))
retobj()
