

def outer():

    def inner():

        print("In Inner function")

    inner()
    print("In outer function")

print("Start code")
outer()
print("End code")
