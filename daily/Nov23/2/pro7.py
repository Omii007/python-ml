

def outer():

    def inner1(x,y):
        print("In Inner function")
        return x+y

    def inner2(a,b):
        print("In Inner2 function")
        return a*b

    return inner1,inner2

inn1,inn2 = outer()

'''for i in ret:
    retval = i(5,7)
    print(retval)
'''

ret1 = inn1(7,5)
ret2 = inn2(5,10)

print(ret1+ret2)

print(ret1)
print(ret2)
