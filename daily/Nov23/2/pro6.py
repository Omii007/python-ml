

def outer():

    def inner1():

        print("In inner1 function")

    def inner2():

        print("In Inner2 function:")

    print("In outer function")
    return inner1,inner2

ret = outer()

for i in ret:
    print(i)
    i()
