

def outer():

    inner()  # UnboundLocalError: local variable 'inner' referenced before assignment
    def inner():

        print("In Inner function")

    print("In outer function")

print("Start code")
outer()
print("End code")
