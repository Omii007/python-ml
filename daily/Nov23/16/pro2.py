
player = ["Rohit","Shubman","Virat","Shreyas","KLRahul"]

#count
print(player.count("virat"))

#index
print(player.index("Virat"))

#reverse
player.reverse()
print(player)

#sort
player.sort()
print(player)

#copy
bat = player.copy()
print(bat)
