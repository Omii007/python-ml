
# Methods from list class :
''' Access
1) append
2)extend
3) index
'''

player = ["Rohit","Shubman","Virat","Shreyas","KLRahul"]

print(player)

#append
player.append("SKY")
print(player)

#extend
player.extend(["Jaddu","Shami","Bumrah","Kuldeep","Siraj"])
print(player)

#index
print(player.index("Siraj"))

#insert
player.insert(5,"Hardik")
print(player)
