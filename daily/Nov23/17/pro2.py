

#Shallow Copy & Deep Copy :
''' nested list sathi ha scenario cha vapar hoto
python internally address store krto
single list aste tevha deep copy hote
nested list aste tevha shallow copy hote
'''

# Shallow Copy

lang = ["CPP","Java","Python",["Go","Rust","Dart"]]

newlist = lang.copy()
print(lang)
print(newlist)

print(id(lang) == id(newlist))

lang[3][1] = "JavaScript"
print(lang)
print(newlist)
