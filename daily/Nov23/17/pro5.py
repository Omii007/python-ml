

# Dictionary : (Key,Value)
''' literal method {KV,KV,KV}
dict constructor({K,V})
'''

player1 = {45:"Rohit",77:"Shubman",18:"Virat",1:"KLRahul",96:"Shreyas"}

print(player1)
print(type(player1))

player2 = dict({45:"Rohit",77:"Shubman",18:"Virat",1:"KLRahul",96:"Shreyas"})

print(player2)
print(type(player2))
