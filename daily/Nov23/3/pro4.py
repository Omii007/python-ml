

import array as arr

jerNo = arr.array('i',[45,77,18,96,1])

for i in jerNo:
    print(i,end = " ")

print()

'''
jerNo => array
arr => module
array => function
i => typecode(datatype)
[] => collection(multiple element)
'''
