

#include <stdio.h>

void main(){

	int rows,col;

	printf("Enter a rows: \n");
	scanf("%d",&rows);
	
	printf("Enter a col: \n");
	scanf("%d",&col);

	int num = 1;
	int x = rows*col; 
	for(int i=1;i<=rows;i++){
	
		int count = 0;
		for(int j=1;j<=x;j++){
		
			int num1 = num;
			int sum = 0;
			while(num1 != 0){
			
				int rem = num1 % 10;
				sum = sum + rem;
				num1 = num1/10;
			}
			if(num % sum == 0){
				printf("%d ",num);
				count++;
			}	
			num++;
			if(count == rows)
				break;
		}
		printf("\n");
	}
}
