

# Named Argument :
# Passing variable number of argument to a function.
# varargs
# *argv =====> <class'tuple'>
def fun(*argv):

    for i in argv:
        print(i)
    print(type(argv))
fun(10,20,30,40)
