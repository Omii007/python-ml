
''' Continue Statement :

    pass => pass phkt jya body chya aat ahe tevdha part skip krto.

    continue => continue jya body chya aat ahe tyaphudil all skip krto.

    break => direct loop ch stop krto.

    assert => wrong scenario catch krto.
'''

name = input("Enter your name: ")
age = int(input("Enter your age: "))

assert age > 0," age -ve nasave" # neagative age del ki Assertion Error yete 

if(age > 18):
    print(name," eligible for votting")
else:
    print(name," not eligible for voting")

