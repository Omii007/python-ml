
# Return Statement :

def fact(num):

    mult = 1

    for i in range(1,num+1):

        mult *=i
    return mult

num = int(input("Enter a number: "))

factorial = fact(num)

print("Factorial of {} is {}".format(num,factorial))
