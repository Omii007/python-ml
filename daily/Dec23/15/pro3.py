
# Class Variables or Static Variables :

'''
- class chya aata ekhada variable sapdat nasel tr 'AttributeError' yete.
- class madhil functions la 'methods' mhantat.
- class madhil variables and methods na Attribute mhantat. 
- class chya bhaher ekhada variable sapdat nasel tr 'NameError' yete.
'''

'''
- class madhil variables la class variable mntat.
- te kontya he methods chya aata nastil tr te global variables astat.
- global variables la jaga or store 'class name space' madhe milte.
- class name space madhe initialize zalyvr object kade tyachi copy pathvli jate indivisual
'''


class Company :

    compName = "Facebook"

    def __init__(self):
        print("In constructor")
        self.empId = 12
        self.empName = "Kanha"

    def compInfo(self):
        print(self.empId)
        print(self.empName)
        print(self.compName)

obj = Company()
obj.compInfo()
