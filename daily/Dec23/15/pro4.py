
# Class Variables or Static Variables :

'''
- class chya aata ekhada variable sapdat nasel tr 'AttributeError' yete.
- class madhil functions la 'methods' mhantat.
- class madhil variables and methods na Attribute mhantat. 
- class chya bhaher ekhada variable sapdat nasel tr 'NameError' yete.
'''

'''
- class madhil variables la class variable mntat.
- te kontya he methods chya aata nastil tr te global variables astat.
- global variables la jaga or store 'class name space' madhe milte.
- class name space madhe initialize zalyvr object kade tyachi copy pathvli jate indivisual
- jr object chya navane class ch variable change kela tr to change phkt tya object purta asto. to dusrya konala hi desat nahi.
- jr class chya navane class ch variable change kela tr to change individual object la mnjech pratyek object la desto.
- pratyek object or class kade variables cha mnje object or tya box cha address asto.
'''


class Company :

    compName = "Facebook"

    def __init__(self):
        print("In constructor")
        self.empId = 12
        self.empName = "Kanha"

    def compInfo(self):
        print(self.empId)
        print(self.empName)
        print(self.compName)

obj1 = Company()
obj2 = Company()

obj1.compInfo()
obj2.compInfo()

obj1.compName = "Meta"

obj1.compInfo()
obj2.compInfo()
