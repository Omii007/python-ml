
# 23
# Constructor :

class Employee :
    def __init__(self):         # constructor
        print("In constructor")
        self.empId = 12         # instance variable
        self.empName = "Kanha"

    def empInfo(self):          # instance method
        print(self.empId)
        print(self.empName)

emp1 = Employee()               # object
emp2 = Employee()

emp1.empInfo()                  # methods call
emp2.empInfo()
