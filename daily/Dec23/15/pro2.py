
# Constructor and Instance Variables :

class Employee :

    def __init__(self,empId,empName):
        print("In constructor")
        self.empId = empId
        self.empName = empName

    def empInfo(self):
        print("Employee Id : {}".format(self.empId))
        print("Employee name : {}".format(self.empName))

emp1 = Employee(12,"Kanha")
emp2 = Employee(15,"Ashish")

emp1.empInfo()
emp2.empInfo()
