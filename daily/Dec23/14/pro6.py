
# Constructor
''' Object create kela ki first __new__ la call jato memory allocation sathi.
- tyanantr ek return chi line lehavi lagte tevhach __init__ la call jato.
- te line ahe ' return super().__new__(self) or object.__new__(self)
- __init__(self) ha class cha constructor asto.
- ya constructor cha vapar or use Instance variable initlize krnyasathi hoto.
'''

class Company :

    def __new__(self):
        print("Memory allocation")

        return super().__new__(self)

    def __init__(self):
        print("Init")

obj = Company()

