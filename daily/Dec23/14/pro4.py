
''' User define class cha class name <class type> ahe
type class la ch meta class mntat.
__new__(self) ha memory allocate krun deto
__init__(self) ha memory madhe initialize krto.
'''

class Employee :

    def __new__(self):
        print("Memory Allocation")
        self.x = 10
        self.y = 20

        return super().__new__(self)

    def __init__(self):
        print("Init")

obj = Employee()
